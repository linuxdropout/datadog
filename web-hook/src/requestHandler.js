const fetch = require('node-fetch')
const { providers } = require('./config')
const { MockApiClient } = require('./mockApiLib')

const logger = console

const client = new MockApiClient(providers)

function requestHandler(req, res) {
  const {
    callbackUrl,
    provider,
  } = req.body

  const apiCallback = async apiJsonData => {
    let response

    try {
      response = await fetch(
        callbackUrl,
        {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(apiJsonData),
        },
      )
    } catch (err) {
      logger.error(err)
      logger.error(`Error making request to callbackUrl, possible invalid url supplied: ${callbackUrl}`)
      logger.info(`${callbackUrl} will not recieve data from ${provider}, unsubscribing...`)
      client.unsubscribe(provider, apiCallback)
      return
    }

    if (response.ok) {
      logger.info(`${callbackUrl} was sent data from ${provider}, unsubscribing...`)
      client.unsubscribe(provider, apiCallback)
    } else {
      throw new Error(`Response from callbackUrl was ${response.statusCode} ${response.statusText}, will retry`)
    }
  }
  client.subscribe(provider, apiCallback)

  return res
    .status(200)
    .json({ hello: 'world' })
    .end()
}

module.exports = requestHandler
