const oneSecondInMs = 1000
const twelveHoursInMs = oneSecondInMs * 60 * 60 * 12

module.exports = {
  providers: [
    {
      endpoint: 'http://mock-server:3000/providers/gas',
      name: 'gas',
      baseInterval: 500,
      intervalFallOffFactor: 2,
      maxInterval: twelveHoursInMs,
    },
    {
      endpoint: 'http://mock-server:3000/providers/internet',
      name: 'internet',
      baseInterval: 500,
      intervalFallOffFactor: 2,
      maxInterval: twelveHoursInMs,
    },
  ],
}
