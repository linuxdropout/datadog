const logger = console
const fetch = require('node-fetch')

/**
 * @typedef Provider
 * @property {string} endpoint
 * @property {string} name
 * @property {number} baseInterval - starting retry between requests to endpoint
 * @property {number} intervalFallOffFactor - factor to multiply interval by on each fetch attempt
 * @property {number} maxInterval - max time between retries
 */

class MockApiClient {
  /**
   * @param {Provider[]} providers
   */
  constructor(providers) {
    this.providerHashTable = {}

    for (const provider of providers) {
      this.providerHashTable[provider.name] = {
        ...provider,
        currentInterval: provider.baseInterval,
        subscriptions: new Set(),
      }
    }
  }

  subscribe(providerName, callback) {
    const provider = this.providerHashTable[providerName]

    provider.subscriptions.add(callback)
    logger.info(`Registered subcription to provider ${providerName}`)

    this.fetchUpdate(providerName)
  }

  unsubscribe(providerName, callback) {
    const provider = this.providerHashTable[providerName]
    provider.subscriptions.delete(callback)

    if (provider.subscriptions.size === 0) {
      provider.updating = false
    }
  }

  fetchUpdate(providerName) {
    const provider = this.providerHashTable[providerName]

    if (provider.updating) return
    provider.updating = true
    this.pollForUpdate(providerName)
  }

  async pollForUpdate(providerName) {
    const provider = this.providerHashTable[providerName]
    if (!provider.updating) return

    try {
      logger.info(`Fetching response from ${provider.endpoint}`)
      const response = await fetch(provider.endpoint)
      if (!response.ok) {
        throw new Error(`${response.statusCode} ${response.statusText} response from provider ${providerName}`)
      }
      const body = await response.json()

      if (body) {
        let errorInSubscriber = false

        for (const callback of provider.subscriptions) {
          try {
            await callback(body)
          } catch (err) {
            logger.warn(err)
            errorInSubscriber = true
          }
        }

        if (errorInSubscriber) {
          // error in subscriber, keep polling
          throw new Error('Error in at least one subscriber')
        }
        provider.updating = false
      }
    } catch (err) {
      logger.warn(err)
      setTimeout(
        () => {
          this.pollForUpdate(providerName)
        },
        provider.currentInterval,
      )
      provider.currentInterval = Math.min(
        provider.currentInterval * provider.intervalFallOffFactor,
        provider.maxInterval,
      )
    }
  }
}

module.exports = {
  MockApiClient,
}
