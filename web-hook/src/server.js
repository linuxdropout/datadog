const express = require('express')
const multer = require('multer')

const requestHandler = require('./requestHandler')
const requestValidator = require('./requestValidator')

const logger = console

function main() {
  const port = process.env.PORT || 3000

  express()
    .use(multer().array())
    .use(requestValidator)
    .use(requestHandler)
    .listen(
      port,
      err => {
        if (err) logger.error(err)
        logger.info(`Server listening on ${port}`)
      },
    )
}

if (require.main === module) main(...process.argv)
else module.exports = main
