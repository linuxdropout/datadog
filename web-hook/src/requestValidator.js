const { providers } = require('./config')

const logger = console
const allowedProviders = providers.map(provider => provider.name)

function requestValidator(req, res, next) {
  if (req.method === 'OPTIONS') {
    return res
      .status(200)
      .end()
  }

  if (req.method !== 'POST') {
    logger.warn(`Recieved ${req.method} request, ending response with 405 status`)
    return res
      .status(405)
      .end()
  }

  if (!req.body) {
    logger.warn('Missing request body, ending response with 400 status')
    return res.status(400).end()
  }

  if (!req.body.callbackUrl) {
    logger.warn('Recieved missing callbackUrl, ending response with 400 status')
    return res.status(400).send('Missing callbackUrl').end()
  }

  if (!req.body.provider) {
    logger.warn('Recieved missing provider, ending response with 400 status')
    return res.status(400).send('Missing provider').end()
  }

  if (!allowedProviders.includes(req.body.provider)) {
    logger.warn(`Recieved unknown provider: ${req.body.provider}, ending response with 404 status`)
    return res.status(404).send('Unknown Provider').end()
  }

  return next()
}

module.exports = requestValidator
