const tape = require('tape')
const sinon = require('sinon')
const proxyquire = require('proxyquire')

tape('mockApiLib :: MockApiClient :: single provider, single subscription', t => {
  const sandbox = sinon.createSandbox()

  const testResponse = { foo: 'bar' }
  const mocks = {
    'node-fetch': sandbox.stub().resolves({
      ok: true,
      json: sandbox.stub().resolves(testResponse),
    }),
  }
  const { MockApiClient } = proxyquire('../../src/mockApiLib', mocks)

  const client = new MockApiClient([
    {
      baseInterval: 0,
      maxInterval: 0,
      endpoint: 'http://test.endpoint',
      intervalFallOffFactor: 0,
      name: 'test-provider',
    },
  ])

  const callback = response => {
    t.deepEqual(response, testResponse)
    client.unsubscribe('test-provider', callback)

    t.equal(mocks['node-fetch'].callCount, 1, 'Makes single fetch request')
    const { args } = mocks['node-fetch'].getCall(0)
    t.deepEqual(
      args,
      ['http://test.endpoint'],
      'Endpoint is provider endpoint',
    )

    sandbox.restore()
    t.end()
  }

  client.subscribe('test-provider', callback)
})

tape('mockApiLib :: MockApiClient :: single provider, multiple subscriptions', t => {
  const sandbox = sinon.createSandbox()

  const testResponse = { foo: 'bar' }
  const mocks = {
    'node-fetch': sandbox.stub().resolves({
      ok: true,
      json: sandbox.stub().resolves(testResponse),
    }),
  }
  const { MockApiClient } = proxyquire('../../src/mockApiLib', mocks)

  const client = new MockApiClient([
    {
      baseInterval: 0,
      maxInterval: 0,
      endpoint: 'http://test.endpoint',
      intervalFallOffFactor: 0,
      name: 'test-provider',
    },
  ])

  t.plan(9)

  const callback1 = response => {
    t.deepEqual(response, testResponse, 'response is as expected for callback1')
    client.unsubscribe('test-provider', callback1)

    t.equal(mocks['node-fetch'].callCount, 1, 'Makes single fetch request')
    const { args } = mocks['node-fetch'].getCall(0)
    t.deepEqual(
      args,
      ['http://test.endpoint'],
      'Endpoint is provider endpoint for callback1',
    )
  }
  const callback2 = response => {
    t.deepEqual(response, testResponse, 'response is as expected for callback2')
    client.unsubscribe('test-provider', callback2)

    t.equal(mocks['node-fetch'].callCount, 1, 'Makes single fetch request')
    const { args } = mocks['node-fetch'].getCall(0)
    t.deepEqual(
      args,
      ['http://test.endpoint'],
      'Endpoint is provider endpoint for callback2',
    )
  }
  const callback3 = response => {
    t.deepEqual(response, testResponse, 'response is as expected for callback3')
    client.unsubscribe('test-provider', callback2)

    t.equal(mocks['node-fetch'].callCount, 1, 'Makes single fetch request')
    const { args } = mocks['node-fetch'].getCall(0)
    t.deepEqual(
      args,
      ['http://test.endpoint'],
      'Endpoint is provider endpoint for callback3',
    )
  }

  client.subscribe('test-provider', callback1)
  client.subscribe('test-provider', callback2)
  client.subscribe('test-provider', callback3)
})

tape('mockApiLib :: MockApiClient :: handles repeated errors', t => {
  const sandbox = sinon.createSandbox()
  sandbox.stub(console, 'warn').returns()

  const testResponse = { foo: 'bar' }
  const mocks = {
    'node-fetch': sandbox.stub().resolves({ ok: false }),
  }

  mocks['node-fetch'].onCall(999).resolves({
    ok: true,
    json: sandbox.stub().resolves(testResponse),
  })

  const { MockApiClient } = proxyquire('../../src/mockApiLib', mocks)

  const client = new MockApiClient([
    {
      baseInterval: 0,
      maxInterval: 0,
      endpoint: 'http://test.endpoint',
      intervalFallOffFactor: 0,
      name: 'test-provider',
    },
  ])

  const callback = response => {
    t.deepEqual(response, testResponse)
    client.unsubscribe('test-provider', callback)

    t.equal(mocks['node-fetch'].callCount, 1000, 'Makes 1000 attempts')
    const { args } = mocks['node-fetch'].getCall(0)
    t.deepEqual(
      args,
      ['http://test.endpoint'],
      'Endpoint is provider endpoint',
    )

    sandbox.restore()
    t.end()
  }

  client.subscribe('test-provider', callback)
})
