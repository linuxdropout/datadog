const tape = require('tape')
const supertest = require('supertest')

const webhookHost = 'http://localhost:3002'

// https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods
// CONNECT is skipped due to socket hang-up errors
const forbiddenHttpMethods = [
  'GET',
  'HEAD',
  'PUT',
  'DELETE',
  'TRACE',
  'PATCH',
]

for (const httpMethod of forbiddenHttpMethods) {
  tape(`Should return status 405 to a ${httpMethod} request`, t => {
    const app = supertest(webhookHost)

    app[httpMethod.toLowerCase()]('/')
      .expect(405)
      .end(err => {
        if (err) t.error(err)
        t.pass(httpMethod)

        return t.end()
      })
  })
}

tape('Should return status 200 to an OPTIONS request', t => {
  const app = supertest(webhookHost)

  app
    .options('/')
    .expect(200)
    .end(err => {
      if (err) t.error(err)
      t.pass('OPTIONS')

      return t.end()
    })
})

const validTestCases = [
  { provider: 'gas', callbackUrl: 'http' },
  { provider: 'internet', callbackUrl: 'bar' },
]
for (const { provider, callbackUrl } of validTestCases) {
  tape('Should return status 200 json to a valid provider and callbackUrl', t => {
    const app = supertest(webhookHost)

    app
      .post('/')
      .field('provider', provider)
      .field('callbackUrl', callbackUrl)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(err => {
        if (err) t.error(err)
        t.pass(`provider:${provider}, callbackUrl:${callbackUrl}`)

        return t.end()
      })
  })
}

const invalidTestCases = [
  { expectedStatusCode: 400, provider: 'internet', callbackUrl: '' },
  { expectedStatusCode: 404, provider: 'foo', callbackUrl: 'foo' },
  { expectedStatusCode: 404, provider: 0, callbackUrl: 'foo' },
  { expectedStatusCode: 404, provider: 123, callbackUrl: 'foo' },
  { expectedStatusCode: 404, provider: false, callbackUrl: 'foo' },
]
for (const { provider, callbackUrl, expectedStatusCode } of invalidTestCases) {
  tape(`Should return status ${expectedStatusCode} to an invalid provider or callbackUrl`, t => {
    const app = supertest(webhookHost)

    app
      .post('/')
      .field('provider', provider)
      .field('callbackUrl', callbackUrl)
      .expect(expectedStatusCode)
      .end(err => {
        if (err) t.error(err)
        t.pass(`provider:${provider}, callbackUrl:${callbackUrl}`)

        return t.end()
      })
  })
}

tape('Should return status 400 to a missing callbackUrl', t => {
  const app = supertest(webhookHost)

  app
    .post('/')
    .field('provider', 'internet')
    .expect(400)
    .end(err => {
      if (err) t.error(err)
      t.pass('provider:internet, callbackUrl:missing')

      return t.end()
    })
})

tape('Should return status 400 to a missing provider', t => {
  const app = supertest(webhookHost)

  app
    .post('/')
    .field('callbackUrl', 'foobar')
    .expect(400)
    .end(err => {
      if (err) t.error(err)
      t.pass('provider:missing, callbackUrl:foobar')

      return t.end()
    })
})
