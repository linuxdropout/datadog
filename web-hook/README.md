# Stream of Consciousness

- Webhook payload containts 2 "fields". Am I expecting JSON fields or form fields?
  - will start with JSON as easiest
  - query params could also be a thing
  - will add more if I have time
  - ended up using form fields afterall as supertest made life easy

- Integration tests will be hard with an indefinite timeout
  - will add a timeout as an environment variable for testing

- At this point I'd normally want to organise a DA or at the very least rubber-ducking session with another dev to go over the proposed subscription model in my head
  - Probably draw up some diagrams etc
  - But given the nature of the task, I'm just gonna dive in coding
  - Unit tests will be my friend here
  - Making assumption: single provider has single endpoint. In reality probably one-to-many or event many-to-many

- Fetch might not follow re-directs, a test-case worth adding probably
- Feeling like there's a million other things I'd like to do around bullet-proofing such as:
  - Integration test with a second server that emits intermittent errors
  - Unit test coverage around setTimout checking intervals increment correctly
  - General unit test coverage around requests
  - Url validation at the point of recieving callbackUrl - rather than on trying to use it
  - But in general pretty happy that I've dipped into all the areas I could go into
  - Only thing I don't like is a lack of coherant pattern between services and how things are required by eachother
  - ^ This is where the previously mentioned DA and archicture diagrams would help a lot

- Had a think that there's 2 key problems with my solution
  1. It doesn't scale
  2. It relies on in-memory data and won't survive a restart
  - So I added a diagram for a scalable solution that solves these issues
