module.exports = {
  env: {
    commonjs: true,
    es6: true,
    node: true,
  },
  extends: [
    'airbnb-base',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 11,
  },
  rules: {
    'generator-star-spacing': 'off',
    'prefer-destructuring': ['error',
      {
        VariableDeclarator: {
          array: false,
          object: true
        },
        AssignmentExpression: {
          array: false,
          object: false
        }
      },
      {
        enforceForRenamedProperties: false
      }
    ],
    'no-await-in-loop': 0,
    'no-restricted-syntax': 0,
    'arrow-parens': ['error', 'as-needed'],
    'no-unneeded-ternary': 'error',
    'semi': 0,
    'no-var': 'error',
    'prefer-const': 'error',
    'curly': ['error', 'multi-line', 'consistent'],
    'max-statements-per-line': ['error', {
      'max': 1
    }],
    'quote-props': ['error', 'consistent-as-needed'],
    'no-useless-rename': 'error',
    'linebreak-style': [
      'error',
      'unix'
    ],
  },
}
